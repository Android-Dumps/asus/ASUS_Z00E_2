#!/system/bin/sh
if ! applypatch -c EMMC:/dev/block/bootdevice/by-name/recovery:11053056:8b1c069081966812bcbbf241757ee64a92afe616; then
  applypatch -b /system/etc/recovery-resource.dat EMMC:/dev/block/bootdevice/by-name/boot:10399744:ea81840b0492b005e7ba5f89749ea5f2d83d1743 EMMC:/dev/block/bootdevice/by-name/recovery 8b1c069081966812bcbbf241757ee64a92afe616 11053056 ea81840b0492b005e7ba5f89749ea5f2d83d1743:/system/recovery-from-boot.p && log -t recovery "Installing new recovery image: succeeded" || log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
